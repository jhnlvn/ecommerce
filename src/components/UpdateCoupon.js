import {useState,useEffect,useContext,Fragment} from 'react';
import {Container, Row, Col, Card, Button, Form} from 'react-bootstrap';
import {useParams,useNavigate,Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext.js';

export default function UpdateCoupon(){

	const {user}=useContext(UserContext);

	const [discount, setDiscount] = useState("");
	const [status, setStatus] = useState("");

	const navigate = useNavigate();

	const {couponCode} = useParams();

	function updateCoupon(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/coupon/edit/${couponCode}`, 
		{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				discount: discount
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Updated Coupon!",
					icon: "success",
					text: "See!"
				})

				navigate('/dashboard');
			} else {

				Swal.fire({
					title: "Failed!",
					icon: "success",
					text: "No admin accounts is allowed to place an order!"
				})

				navigate('/dashboard');
			}
		})
	}

	return (
		<Fragment>
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{couponCode}</Card.Title>
                            <Card.Subtitle>Discount:</Card.Subtitle>
                            <Form className="mx-auto col-2">
						    	<Form.Group className="my-3" controlId="formQuantity">
			      		 			<Form.Control className="text-center" type="number" min="0" onChange={event => setDiscount(event.target.value)} required/>
						    	</Form.Group>
	    					</Form>
	    						<Button variant="primary" onClick={event => updateCoupon(event)}>Update</Button>
                        </Card.Body>    
                    </Card>
                </Col>
            </Row>
        </Container>
        </Fragment>
		)
}