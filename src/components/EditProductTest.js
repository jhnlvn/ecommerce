import {Col, Button, Card, Form} from 'react-bootstrap';
import {useState,useEffect,useContext,Fragment} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';

import Swal from 'sweetalert2';

export default function EditProduct({editProp}) {

	const {_id} = editProp;

	const [product, setProduct] = useState({_id});
	const [image,setImage] = useState('');
	const [name,setName] = useState('');
	const [category,setCategory] = useState('');
	const [description,setDescription] = useState('');
	const [price,setPrice] = useState('');
	const [stocks,setStocks] = useState('');
	const [discount,setDiscount] = useState('');

	const [isActive,setIsActive] = useState(false);

	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/product/${_id}`)
		.then(result => result.json())
		.then(data => {
			setProduct(data._Id);
			setImage(data.image);
			setName(data.name);
			setCategory(data.category);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
			setDiscount(data.itemDiscount);
		})
			
	}, [])

	useEffect(() => {
		if(setImage !== "" && setName !== "" && setDescription !== "" && setCategory !== "" && setPrice !== "" && setStocks !== "" && setDiscount !== ""){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [setProduct,setImage,setName,setDescription,setCategory,setPrice,setStocks,setDiscount])

	const edit = (product) => {
		fetch(`${process.env.REACT_APP_API_URL}/product/update/${product}`, 
		{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				image: image,
				description: description,
				category: category,
				price: price,
				itemDiscount: discount,
				stocks: stocks
			})
		})
		.then(result => result.json())
		.then(data => {

			if(data === false){
				Swal.fire({
					title: 'Authentication failed!',
					icon: 'error',
					text: 'Please try again!'
				})
			}else{

				Swal.fire({
					title: 'Authentication successful!',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})

				navigate("/");
			}

		})
	}

	return (
		<Fragment>
				<h1 className="text-center mt-5">{product}</h1>
			    <Form className="mt-5" onSubmit={event => edit(event)}>

			      <Form.Group className="mb-3" controlId="form-name">
			        <Form.Label>Product Name:</Form.Label>
			        <Form.Control
			        	type="text" 
			        	value={name}
			        	onChange={event => setName(event.target.value)}
			        	required />
			        <Form.Text className="text-muted">
			          {name}
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="form-description">
			        <Form.Label>Product Description:</Form.Label>
			        <Form.Control
			        	type="text" 
			        	value={description}
			        	onChange={event => setDescription(event.target.value)}
			        	required />
			        <Form.Text className="text-muted">
			          {description}
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="form-category">
			        <Form.Label>Product Category:</Form.Label>
			        <Form.Control
			        	type="text" 
			        	value={category}
			        	onChange={event => setCategory(event.target.value)}
			        	required />
			        <Form.Text className="text-muted">
			          {category}
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="form-image">
			        <Form.Label>Product Image:</Form.Label>
			        <Form.Control
			        	type="text" 
			        	value={image}
			        	onChange={event => setImage(event.target.value)}
			        	required />
			        <Form.Text className="text-muted">
			          {image}
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="form-stocks">
			        <Form.Label>Product Stocks:</Form.Label>
			        <Form.Control
			        	type="number" 
			        	value={stocks}
			        	onChange={event => setStocks(event.target.value)}
			        	required />
			        <Form.Text className="text-muted">
			          {stocks}
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="form-price">
			        <Form.Label>Product Price:</Form.Label>
			        <Form.Control
			        	type="number" 
			        	value={price}
			        	onChange={event => setPrice(event.target.value)}
			        	required />
			        <Form.Text className="text-muted">
			          {price}
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="form-discount">
			        <Form.Label>Product Discount:</Form.Label>
			        <Form.Control
			        	type="number" 
			        	value={discount}
			        	onChange={event => setDiscount(event.target.value)}
			        	required />
			        <Form.Text className="text-muted">
			          {discount}
			        </Form.Text>
			      </Form.Group>

			      {
			      	isActive ?
			      	<Button variant="primary" type="submit">
			      	  Login
			      	</Button>
			      	:
			      	<Button variant="danger" type="submit" disabled>
			      	  Login
			      	</Button>
			      }

			    </Form>
		</Fragment>
		)
}