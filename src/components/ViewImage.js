import {useState,useEffect,useContext,Fragment} from 'react';
import {Container, Row, Col, Card, Button, Form} from 'react-bootstrap';
import {useParams,useNavigate,Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext.js';

export default function ViewImage(imageProp){

	const {user}=useContext(UserContext);

	const navigate = useNavigate();

	return (
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
   				   <Card.Img className="img-fluid" variant="top" src={imageProp} />
                        <Card.Body className="text-center">
                            <Card.Title></Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text></Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP </Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text>
                            <Card.Subtitle>Quantity</Card.Subtitle>
	    					<Button variant="primary">Go back to Market page</Button>
                        </Card.Body>    
                    </Card>
                </Col>
            </Row>
        </Container>
		)
}