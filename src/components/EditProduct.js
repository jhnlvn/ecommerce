import {useEffect, useState,} from 'react';
import {useParams} from 'react-router';
import {Form,Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function EditProduct(){

	const [product, setProduct] = useState("");
	const [name, setName] = useState("");
	const [category, setCategory] = useState("");
	const [price, setPrice] = useState("");
	const [stocks, setStocks] = useState("");
	const [description, setDescription] = useState("");
	const [image, setImage] = useState("");
	const [discount, setDiscount] = useState("");

	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	useEffect(() => {
		if(name !== "" && description !== "" && price !== "" && stocks !== "" && category !== "" && image !== "" && discount !== ""){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [name,category,description,stocks,price,image,discount])

	function edit(event){
		event.preventDefault();

		let id = document.querySelector("#form-id").value

		fetch(`${process.env.REACT_APP_API_URL}/product/update/${id}`, {
			method: 'PUT',
			headers: {'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`},
			body: JSON.stringify({
				name: name,
				category: category,
				description: description,
				price: price,
				stocks: stocks,
				discount: discount,
				image: image
			})
		})
		.then(result => result.json())
		.then(data => {

			if(data === false){
				Swal.fire({
					title: 'Updating details failed!',
					icon: 'error',
					text: 'Please try again!'
				})
			}else{
				Swal.fire({
					title: 'Edit Successful!',
					icon: 'success',
					text: 'Product is now updated!'
				})
			}

			document.querySelector("#form-id").value = "";
			document.querySelector("#form-name").value = "";
			document.querySelector("#form-category").value = "";
			document.querySelector("#form-description").value = "";
			document.querySelector("#form-price").value = "";
			document.querySelector("#form-stocks").value = "";
			document.querySelector("#form-discount").value = "";
			document.querySelector("#form-image").value = "";

			navigate("/dashboard");
		})
	}


	function loadData(event){
	document.querySelector("#formProduct").addEventListener("keyup", (event) => {
		event.preventDefault();

		let id = document.querySelector("#form-id").value
	
		fetch(`${process.env.REACT_APP_API_URL}/product/${id}`)
		.then(result => result.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setCategory(data.category);
			setStocks(data.stocks);
			setPrice(data.price);
			setImage(data.image);
			setDiscount(data.itemDiscount);

			document.querySelector("#form-name").value = data.name;
			document.querySelector("#form-category").value = data.category;
			document.querySelector("#form-description").value = data.description;
			document.querySelector("#form-price").value = data.price;
			document.querySelector("#form-stocks").value = data.stocks;
			document.querySelector("#form-discount").value = data.itemDiscount;
			document.querySelector("#form-image").value = data.image;
		})
	})
}
	return (
		
		<Form className="mt-1 pb-3" onSubmit={event => edit(event)}>
			<h3 className="text-center">Edit a product</h3>
		  <Form.Group className="mb-2" id="formProduct">
		    <Form.Label>Product ID:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Enter a product name"
		    	id="form-id"
		    	onChange={event => setProduct(event.target.value)}
		    	onChange={event => loadData(event)} 
		    	required />

		    </Form.Group>
		  <Form.Group className="mb-2" id="formDetails">
		    <Form.Label>Product Name:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Enter a product name"
		    	id="form-name"
		    	onChange={event => setName(event.target.value)}
		    	required />

		    <Form.Label>Category:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Category of your product"
		    	id="form-category"
		    	onChange={event => setCategory(event.target.value)}
		    	required />

		    <Form.Label>Description:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Description of the product"
		    	id="form-description"
		    	onChange={event => setDescription(event.target.value)}
		    	required />

		    <Form.Label>Price:</Form.Label>
		    <Form.Control
		    	type="number" 
		    	placeholder="How much is your product?"
		    	id="form-price"
		    	onChange={event => setPrice(event.target.value)}
		    	required />

		    <Form.Label>Stocks:</Form.Label>
		    <Form.Control
		    	type="number" 
		    	placeholder="How many stocks do you have?"
		    	id="form-stocks"
		    	onChange={event => setStocks(event.target.value)}
		    	required />

		    <Form.Label>Image URL:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Image url of the product"
		    	id="form-image"
		    	onChange={event => setImage(event.target.value)}
		    	required />

		    <Form.Label>Discount:</Form.Label>
		    <Form.Control
		    	type="number" 
		    	placeholder="Do you have discount?"
		    	id="form-discount"
		    	onChange={event => setDiscount(event.target.value)}
		    	required />
		  </Form.Group>

		  {
		  	isActive ?
		  	<Button variant="primary" type="submit">
		  	  Update
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" disabled>
		  	  Update
		  	</Button>
		  }

		</Form>
	)
}