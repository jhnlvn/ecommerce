import {Col, Button, Card} from 'react-bootstrap';
import {useState,useEffect,useContext} from 'react';

import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';
import EditProduct from '../components/EditProduct.js';
import ViewImage from '../components/ViewImage.js';

export default function CouponList({couponProp}){

	const {couponCode,couponDiscount,isActive} = couponProp;

	const [status,setStatus] = useState('');

	const {user} = useContext(UserContext);

	useEffect(()=>{
		if(couponProp.isActive){
			setStatus("Active");
		}else{
			setStatus("Inactive");
		}

	}, [setStatus])


	function changeCoupon(event){
		event.preventDefault();

		let active = false;

		if(status === "Active" ){
			setStatus("Inactive");
			active = false
		}else{
			setStatus("Active");
			active = true
		}

		fetch(`${process.env.REACT_APP_API_URL}/coupon/archive`, 
		{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				code: couponCode, 
				status: active
			})
		})
		.then(result => result.json())
		.then(data => data)
	}


	return(
		<tr>
		<td>{couponCode}</td>
		<td>{couponDiscount}</td>
		<td>{status} <Button onClick={event => changeCoupon(event)}>change</Button>	</td>
		<td><Button as = {Link} to ={`/coupon/${couponCode}`}>Edit</Button></td>
		</tr>
	)
}