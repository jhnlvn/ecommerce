import {Col, Button, Card} from 'react-bootstrap';
import {useState,useEffect,useContext} from 'react';

import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';
import EditProduct from '../components/EditProduct.js';

export default function DashboardItems({userProp}){

	const {_id,email,isAdmin} = userProp;

	const [status, setStatus] = useState()

	useEffect(()=>{
		setStatus(isAdmin)
	},[setStatus])


	const adminStatus = (email) => {
		if(!status){

		fetch(`${process.env.REACT_APP_API_URL}/user/setadmin`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				email: email
			})

		})
		.then(result => result.json())
		.then(data => data)

		setStatus(true)
		}
		else{

		fetch(`${process.env.REACT_APP_API_URL}/user/setnonadmin`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				email: email
			})

		})
		.then(result => result.json())
		.then(data => data)
		}


	}

	return(
		<tr>
		<td>{_id}</td>
		<td>{email}</td>
		<td>
		<Button variant="primary" onClick={() => adminStatus(email)}>Change Account Status</Button>
		</td>
		</tr>
	)
}