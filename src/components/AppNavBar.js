import {Container,Nav,Navbar,Button,Offcanvas,Form} from 'react-bootstrap';

import {Link,NavLink} from 'react-router-dom';
import {useState,useEffect,useContext,Fragment} from 'react';

import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function AppNavbar(){

	const [oldPassword, setOldPassword] = useState("");
	const [newPassword, setNewPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [correctPassword, setCorrectPassword] = useState(false);

	const {user} = useContext(UserContext);

	const [isActive, setIsActive] = useState(false);

	useEffect(() => {

		if(newPassword !== "" && confirmPassword !== "" && newPassword === confirmPassword){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [correctPassword,oldPassword,newPassword,confirmPassword])


  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  function changePassword(event){
  	event.preventDefault();

  	fetch(`${process.env.REACT_APP_API_URL}/user/change-password`, {
  		method: 'PUT',
  		headers: {'Content-Type':'application/json'},
  		body: JSON.stringify({
  			oldPassword: oldPassword,
  			newPassword: newPassword
  		})
  	})
  	.then(result => result.json())
  	.then(data => {

  				Swal.fire({
  					title: 'Registration successful!',
  					icon: 'success',
  					text: 'Welcome to Zuitt!'
  				})

			})
		}


	return(

		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand as = {Link} to = "/">Shapi</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="ms-auto">
		        {
		          user ?
	      		  <Fragment>
		     		 <Nav.Link as = {NavLink} to ="/market">Market</Nav.Link>
		     		 {
		     		 	user.isAdmin ?
			      		  <Fragment>
				     			 <Nav.Link as = {NavLink} to ="/dashboard">Dashboard</Nav.Link>
				     			 <Nav.Link as = {NavLink} to ="/users">Users</Nav.Link>
			     		  </Fragment>
				     		 :
			      		  <Fragment>
				     			 <Nav.Link as = {NavLink} to ="/cart">Cart</Nav.Link>
			     		  </Fragment>
		     		 }
				    <Button variant="light outline-light" className="border-0" onClick={handleShow}>Settings</Button>
					    <Offcanvas show={show} onHide={handleClose} placement="end">
					      <Offcanvas.Header closeButton>
					        <Offcanvas.Title>Change Password</Offcanvas.Title>
					      </Offcanvas.Header>
					      <Offcanvas.Body>
					      	    <Form className="mt-1 pb-3" onSubmit={event => changePassword(event)}>

					      	      <Form.Group className="mb-2" controlId="formCurrentPassword">
					      	        <Form.Label>Old Password</Form.Label>
					      	        <Form.Control
					      	        	type="password"
					      	        	placeholder="Old password"
					      	        	value={oldPassword}
					      	        	onChange={event => setOldPassword(event.target.value)}
					      	        	required />
					      	      </Form.Group>

					      	      <Form.Group className="mb-2" controlId="formChangePassword">
					      	        <Form.Label>New Password</Form.Label>
					      	        <Form.Control
					      	        	type="password"
					      	        	placeholder="New password"
					      	        	value={newPassword}
					      	        	onChange={event => setNewPassword(event.target.value)}
					      	        	required />
					      	      </Form.Group>

					      	      <Form.Group className="mb-2" controlId="formConfirmChangePassword">
					      	        <Form.Label>Confirm Password</Form.Label>
					      	        <Form.Control
					      	        	type="password"
					      	        	placeholder="Confirm your new password"
					      	        	value={confirmPassword}
					      	        	onChange={event => setConfirmPassword(event.target.value)}
					      	        	required />
					      	      </Form.Group>
					      	      {
					      	      	isActive ?
					      	      	<Button variant="primary" type="submit">
					      	      	  Change Password
					      	      	</Button>
					      	      	:
					      	      	<Button variant="secondary" type="submit" disabled>
					      	      	  Fill-up the fields
					      	      	</Button>
					      	      }

					      	    </Form>
					      </Offcanvas.Body>
					    </Offcanvas>
	      		 	 <Nav.Link as = {NavLink} to ="/logout">Logout</Nav.Link>
	     		  </Fragment>
	      		  :
	      		  <Fragment>
		       		 <Nav.Link as = {NavLink} to ="/">Home</Nav.Link>
		     		 <Nav.Link as = {NavLink} to ="/market">Market</Nav.Link>
	     		  </Fragment>
		        }
		       
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		)
}