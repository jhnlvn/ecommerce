import {Col, Button, Card} from 'react-bootstrap';
import {useContext} from 'react';

import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';

export default function ItemCard({productProp}){

	const {_id,image,name,description,price,itemsSold,stocks} = productProp;

	const {user} = useContext(UserContext);

	return(

			<Col className="col-3 mb-3">
				<Card className="cardHeight">
   				   <Card.Img className="img-fluid" variant="top" src={image} />
			      <Card.Body>
			        <Card.Title className="text-center">{name}</Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>{description}</Card.Text>
			        <Card.Subtitle>Price: PhP {price}.00</Card.Subtitle>
			        <Card.Text></Card.Text>
			        <Card.Subtitle></Card.Subtitle>
			        <Card.Text className="text-muted">Stocks: {stocks} | Sold items: {itemsSold}</Card.Text>
			        {
			        	user ?
       					<Button as = {Link} to ={`/viewitem/${_id}`} className="offset-1 col-10">View Item</Button>
       					:
       					<Button as = {Link} to ='/'>Login to order</Button>
			        }
			      </Card.Body>
			    </Card>
			</Col>
	)
}