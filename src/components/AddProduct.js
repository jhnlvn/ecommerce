import {useEffect, useState} from 'react';
import {Form,Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function AddProduct(){

	const [newProductName, setNewProductName] = useState("");
	const [newProductCategory, setNewProductCategory] = useState("");
	const [newProductPrice, setNewProductPrice] = useState("");
	const [newProductStocks, setNewProductStocks] = useState("");
	const [newProductDescription, setNewProductDescription] = useState("");
	const [newProductImage, setNewProductImage] = useState("https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png");

	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(newProductName !== "" && newProductDescription !== "" && newProductPrice !== "" && newProductStocks !== "" && newProductCategory !== ""){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [newProductName,newProductPrice,newProductStocks,newProductDescription,newProductCategory])

	function add(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/product/add`, {
			method: 'POST',
			headers: {'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`},
			body: JSON.stringify({
				name: newProductName,
				category: newProductCategory,
				description: newProductDescription,
				price: newProductPrice,
				stocks: newProductStocks,
				image: newProductImage
			})
		})
		.then(result => result.json())
		.then(data => {

			if(data === false){
				Swal.fire({
					title: 'Adding product failed!',
					icon: 'error',
					text: 'Please try again!'
				})
			}else{
				Swal.fire({
					title: 'Product Creation Success!',
					icon: 'success',
					text: 'Product has been added!'
				})
			}

			setNewProductName('');
			setNewProductCategory('');
			setNewProductDescription('');
			setNewProductPrice('');
			setNewProductStocks('');
			setNewProductImage('');
		})
	}

	return (
		
		<Form className="mt-1 pb-3" onSubmit={event => add(event)}>
			<h3 className="text-center">Add a product</h3>
		  <Form.Group className="mb-2" controlId="formName">
		    <Form.Label>Product Name:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Enter a product name"
		    	value={newProductName}
		    	onChange={event => setNewProductName(event.target.value)}
		    	required />
		  </Form.Group>

		  <Form.Group className="mb-2" controlId="formCategory">
		    <Form.Label>Category:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Category of your product"
		    	value={newProductCategory}
		    	onChange={event => setNewProductCategory(event.target.value)}
		    	required />
		  </Form.Group>

		  <Form.Group className="mb-2" controlId="formDescription">
		    <Form.Label>Description:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Description of the product"
		    	value={newProductDescription}
		    	onChange={event => setNewProductDescription(event.target.value)}
		    	required />
		  </Form.Group>

		  <Form.Group className="mb-2" controlId="formPrice">
		    <Form.Label>Price:</Form.Label>
		    <Form.Control
		    	type="number" 
		    	placeholder="How much is your product?"
		    	value={newProductPrice}
		    	onChange={event => setNewProductPrice(event.target.value)}
		    	required />
		  </Form.Group>

		  <Form.Group className="mb-2" controlId="formStocks">
		    <Form.Label>Stocks:</Form.Label>
		    <Form.Control
		    	type="number" 
		    	placeholder="How many stocks do you have?"
		    	value={newProductStocks}
		    	onChange={event => setNewProductStocks(event.target.value)}
		    	required />
		  </Form.Group>

		  <Form.Group className="mb-2" controlId="formImage">
		    <Form.Label>Image:</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Image url of the product"
		    	value={newProductImage}
		    	onChange={event => setNewProductImage(event.target.value)}
		    	required />
		  </Form.Group>

		  {
		  	isActive ?
		  	<Button variant="primary" type="submit">
		  	  Add
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" disabled>
		  	  Add
		  	</Button>
		  }

		</Form>
	)
}