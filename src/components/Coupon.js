import {Fragment,useEffect, useState,useContext} from 'react';
import {Table,Col,Row,Tab,Tabs,Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';
import CouponList from '../components/CouponList.js';
import {Link} from 'react-router-dom';

export default function Coupon(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [coupons, setCoupons] = useState([]);
	const [archived, setArchived] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/coupon/view`,{
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setCoupons(data.map(coups=>{
				return(
					<CouponList key={coups._id} couponProp={coups}/>
					)
				}))
			})
	}, [])


	return (
		<Fragment>
		<Row className="text-center mb-1 col-1 mx-auto">
		<Button as = {Link} to ={`/coupon/`}>Add</Button>
		</Row>
		<Row>
		<Col>
		<Table className="text-center" responsive striped bordered hover>
		      <thead>
		        <tr>
		          <th>CODE</th>
		          <th>Discount</th>
		          <th>Status</th>
		          <th>Edit</th>
		        </tr>
		      </thead>
		      <tbody>
				{coupons}
		      </tbody>
		    </Table>
		</Col>
    	</Row>
		</Fragment>
		)
}