// destructuring
import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){
	return(

			<Row className="mt-5">
				<Col className="text-center mt-5">
					<h1 className="mt-5">Shapi E-Commerce Website</h1>
					<p className="pt-1">This website was developed using MERN Stack</p>
				</Col>
			</Row>
		)
}