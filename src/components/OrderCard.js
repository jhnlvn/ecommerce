import {Col, Button, Card,ListGroup } from 'react-bootstrap';
import {useContext,useState,useEffect,Fragment} from 'react';

import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';
import ItemList from '../components/ItemList.js'

export default function OrderCard({listProp}){

	const {products,name,address,couponCode,contact,paymentMethod,totalAmount,isPurchased,isDelivered,isReceived} = listProp;

	const {user} = useContext(UserContext);

	const [prod,setProd] = useState([]);

	useEffect(()=>{
		setProd(products.map(item=>{
			return(
				<ItemList key={item.productId} itemProp={item}/>
				)
			}));
	},[])

	return(

		<Col className="col-6 mb-3">
			<Card className="cardHeight">
		      <Card.Body>
		        <Card.Title className="text-center">Receipt</Card.Title>
		        <Card.Subtitle>Contact Name:</Card.Subtitle>
		        <Card.Text>{name}</Card.Text>
		        <Card.Subtitle>Contact Number:</Card.Subtitle>
		        <Card.Text>{contact}</Card.Text>
		        <Card.Subtitle>Delivery Address:</Card.Subtitle>
		        <Card.Text>{address}</Card.Text>
		        <Card.Subtitle>Products:</Card.Subtitle>
		        
		        <ListGroup variant="flush">
		        {prod}
		        </ListGroup>
		        
		        <Card.Subtitle>Payment Method</Card.Subtitle>
		        <Card.Text>{paymentMethod}</Card.Text>
		        <Card.Text>Total Amount: {totalAmount} | Coupon: {couponCode}</Card.Text>
		        {
		        	isReceived ?
   					<Button></Button>
   					:
   						user.isAdmin ?
   							isDelivered ?
   							<Button className="offset-1 col-10"></Button>
   							:
   							<Button className="offset-1 col-10">Order Delivered!</Button>
   						:
   							isDelivered ?
   							<Button className="offset-1 col-10">Order Received!</Button>
   							:
   							<Button className="offset-1 col-10">Cancel Order</Button>
		        }
		      </Card.Body>
		    </Card>
		</Col>
	)
}