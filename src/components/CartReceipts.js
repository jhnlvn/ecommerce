import {Fragment,useEffect, useState} from 'react';
import {Row,Col,Tab,Tabs} from 'react-bootstrap';

import OrderCard from '../components/OrderCard.js';

export default function CartReceipts(){
	const [ordered, setOrdered] = useState([]);
	const [delivered, setDelivered] = useState([]);
	const [received, setReceived] = useState([]);
	const [cancelled, setCancelled] = useState([]);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/order/purchased`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`}
		})
		.then(result => result.json())
		.then(data => {
			setOrdered(data.map(order=>{
				return(
					<OrderCard key={order._id} listProp={order}/>
					)
				}))
			})

		fetch(`${process.env.REACT_APP_API_URL}/order/delivered`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`}
		})
		.then(result => result.json())
		.then(data => {
			setDelivered(data.map(deliver=>{
				return(
					<OrderCard key={deliver._id} listProp={deliver}/>
					)
				}))
			})

		fetch(`${process.env.REACT_APP_API_URL}/order/received`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`}
			})
		.then(result => result.json())
		.then(data => {
			setReceived(data.map(receive=>{
				return(
					<OrderCard key={receive._id} listProp={receive}/>
					)
				}))
			})

		fetch(`${process.env.REACT_APP_API_URL}/order/cancelled`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`}
		})
		.then(result => result.json())
		.then(data => {
			setCancelled(data.map(cancel=>{
				return(
					<OrderCard key={cancel._id} listProp={cancel}/>
					)
				}))
			})

	}, [setOrdered,setDelivered,setReceived,setCancelled])


	return (
		<Fragment>
		<Col>
		<Tabs defaultActiveKey="ordered" id="dashboard-tabs" className="text-center mb-3" fill>
		<Tab eventKey="ordered" title="Ordered">
			<Row>
				{ordered}
			</Row>
		</Tab>
		<Tab eventKey="delivered" title="Delivered">
			<Row>
				{delivered}
			</Row>
		</Tab>
		<Tab eventKey="received" title="Received">
			<Row>
				{received}
			</Row>
		</Tab>
		<Tab eventKey="cancelled" title="Cancelled">
			<Row>
				{cancelled}
			</Row>
		</Tab>
    </Tabs>
    </Col>
		</Fragment>
		)
}