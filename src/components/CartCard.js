import {Row,Col, Card,Button} from 'react-bootstrap';
import {useState,useEffect,useContext} from 'react';
import {Link,useParams,useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext.js';

export default function CartCard({productProp}){
	
	const {productId,quantity,subTotal} = productProp;

	const [product, setProduct] = useState('');
	const [image, setImage] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [remove, setRemove] = useState('');

	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data =>{
			setProduct(data.name);
			setImage(data.image);
			setDescription(data.description);
			setPrice(data.price.toLocaleString());
		})
	}, [remove]);


	function removetocart(event){
		event.preventDefault();

		setRemove(0);

		fetch(`${process.env.REACT_APP_API_URL}/order/add/${productId}`, 
		{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: remove
			})
		})
		.then(result => result.json())
		.then(data => data)
	}

	return(
		<Row>
			<Col className="text-center">
				<Card className="mb-4">
   				   <Card.Img className="img-fluid" variant="top" src={image} />
   				</Card>
   				<Row>
   				<Button as = {Link} to ={`/viewitem/${productId}`} className="mb-2">Change Quantity</Button>
   				</Row>
   				<Row>
                <Button variant="primary" onClick={event => removetocart(event)}>Remove Item</Button>
                </Row>
			</Col>
			<Col className="mb-3">
				<Card className="cardHeight">
			      <Card.Body>
			        <Card.Title>{product}</Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>{description}</Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>PhP {price}</Card.Text>
			        <Card.Subtitle>Quantity:</Card.Subtitle>
			        <Card.Text>{quantity}</Card.Text>
			        <Card.Subtitle>Sub-Total:</Card.Subtitle>
			        <Card.Text>{subTotal}</Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}