import {Col, Button, Card,ListGroup } from 'react-bootstrap';
import {useContext,useEffect,useState} from 'react';

import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';

export default function ItemList({itemProp}){

	const {productId,quantity,subTotal} = itemProp;

	const [prod,setProd] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data => {
			setProd(data.name);
		})
	}, [setProd])

	return(

      <ListGroup.Item>Item: {prod} | Qty.: {quantity} | Subtotal: {subTotal}</ListGroup.Item>

	)
}