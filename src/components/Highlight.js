import Card from 'react-bootstrap/Card';
import {Row, Col,Carousel,Badge} from 'react-bootstrap';
import {useState,useEffect} from 'react';

export default function Hightlight() {

	const [mens,setMens] = useState('')
	const [womens,setWomens] = useState('')
	const [toddlers,setToddlers] = useState('')
	const [appliances,setAppliances] = useState('')
	const [tools,setTools] = useState('')
	const [others,setOthers] = useState('')

	useEffect(()=>{

		fetch(`${process.env.REACT_APP_API_URL}/product/mens`)
		.then(result => result.json())
		.then(data => {
			setMens(data[data.length-1]);
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/womens`)
		.then(result => result.json())
		.then(data => {
			setWomens(data[data.length-1]);
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/toddlers`)
		.then(result => result.json())
		.then(data => {
			setToddlers(data[data.length-1]);
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/appliances`)
		.then(result => result.json())
		.then(data => {
			setAppliances(data[data.length-1]);
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/tools`)
		.then(result => result.json())
		.then(data => {
			setTools(data[data.length-1]);
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/others`)
		.then(result => result.json())
		.then(data => {
			setOthers(data[data.length-1]);
			})

	},[])


	return (

			<Row className="mt-1">
				<Col className="text-center">
				<h4 className="mb-3">HOT DEALS! GET YOURS NOW!</h4>
					{/*<Carousel variant="dark">
					      <Carousel.Item>
					        <img
					          className="img-fluid"
					          src={mens.image}
					        />
					        <Carousel.Caption>
					          <h5>{mens.name} <Badge bg="danger">HOT</Badge></h5>
					          <p>{mens.description}</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="img-fluid"
					          src={womens.image}
					        />
					        <Carousel.Caption>
					          <h5>{womens.name} <Badge bg="danger">HOT</Badge></h5>
					          <p>{womens.description}</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="img-fluid"
					          src={toddlers.image}
					        />
					        <Carousel.Caption>
					          <h5>{toddlers.name} <Badge bg="danger">HOT</Badge></h5>
					          <p>{toddlers.description}</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="img-fluid"
					          src={appliances.image}
					        />
					        <Carousel.Caption>
					          <h5>{appliances.name} <Badge bg="danger">HOT</Badge></h5>
					          <p>{appliances.description}</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="img-fluid"
					          src={tools.image}
					        />
					        <Carousel.Caption>
					          <h5>{tools.name} <Badge bg="danger">HOT</Badge></h5>
					          <p>{tools.description}</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					      <Carousel.Item>
					        <img
					          className="img-fluid"
					          src={others.image}
					        />
					        <Carousel.Caption>
					          <h5>{others.name} <Badge bg="danger">HOT</Badge></h5>
					          <p>{others.description}</p>
					        </Carousel.Caption>
					      </Carousel.Item>
					    </Carousel>*/}
				</Col>
			</Row>
		)
}