import {useState,useEffect,useContext,Fragment} from 'react';
import {Container, Row, Col, Card, Button, Form} from 'react-bootstrap';
import {useParams,useNavigate,Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext.js';

export default function ViewItem(){

	const {user}=useContext(UserContext);

	const [name, setName] = useState("");
	const [image, setImage] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stocks, setStocks] = useState('');
	const navigate = useNavigate();

	const {productId} = useParams();

	useEffect(()=>{

		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setImage(data.image);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
		})
	}, [productId]);

	function addtocart(event){
		event.preventDefault();

		if(quantity<=0){
			Swal.fire({
				title: "Please input a valid number!",
				icon: "error",
				text: "You can put 1 or more!"
			})
		}
		else{
			fetch(`${process.env.REACT_APP_API_URL}/order/add/${productId}`, 
			{
				method: 'PUT',
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					quantity: quantity
				})
			})
			.then(result => result.json())
			.then(data => {
				if(data){
					Swal.fire({
						title: "Item added!",
						icon: "success",
						text: "See!"
					})

					navigate('/cart');
				} else {

					Swal.fire({
						title: "Failed!",
						icon: "success",
						text: "No admin accounts is allowed to place an order!"
					})

					navigate('/market');
				}
			})
		}
	}

	return (
		<Fragment>
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
   				   <Card.Img className="img-fluid" variant="top" src={image} />
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text>
                            <Card.Subtitle>Quantity</Card.Subtitle>
                            <Form className="mx-auto col-2">
						    	<Form.Group className="my-3" controlId="formQuantity">
			      		 			<Form.Control className="text-center" type="number" min="1" max={stocks} onChange={event => setQuantity(event.target.value)} required/>
						    	</Form.Group>
	    					</Form>
	    						<Button variant="primary" onClick={event => addtocart(event)}>Add to cart</Button>
                        </Card.Body>    
                    </Card>
                </Col>
            </Row>
        </Container>
        </Fragment>
		)
}