import {Col, Button, Card} from 'react-bootstrap';
import {useState,useEffect,useContext} from 'react';

import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';
import EditProduct from '../components/EditProduct.js';
import ViewImage from '../components/ViewImage.js';

export default function DashboardItems({productProp}){

	const {_id,name,description,price,itemsSold,stocks,image,category,itemDiscount,createdOn} = productProp;

	const [product, setProduct] = useState([]);

	const {user} = useContext(UserContext);

	const edit = (id) => {
		fetch(`${process.env.REACT_APP_API_URL}/product/${id}`)
		.then(result => result.json())
		.then(data => {
			return (
				<EditProduct key={data._id} editProp={data}/>
				)
		})
	}

	const view = (image) => {
			return 
				<ViewImage imageProp={image}/>
	}

	return(
		<tr>
		<td>{_id}</td>
		<td>{name}</td>
		<td>{description}</td>
		<td>{category}</td>
		<td>{price}</td>
		<td>{stocks}</td>
		<td>{itemsSold}</td>
		<td>{itemDiscount}</td>
		{/*<td>{createdOn}</td>*/}
		{/*<td><Button as = {Link} to ={`/image`}  onClick={() => view(image)} className="offset-1 col-10">View</Button></td>*/}
		<td>
		<Button variant="primary" onClick={() => edit(_id)}>Edit</Button>
		</td>
		</tr>
	)
}