import './App.css';
import {useState,useEffect,Fragment} from 'react';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Market from './pages/Market.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import ViewItem from './components/ViewItem.js';
import ViewImage from './components/ViewImage.js';
import UpdateCoupon from './components/UpdateCoupon.js';
import AddCoupon from './components/AddCoupon.js';
import Dashboard from './pages/Dashboard.js';
import Users from './pages/Users.js';
import Cart from './pages/Cart.js';

import {BrowserRouter as Router,Routes,Route} from 'react-router-dom';

import {UserProvider} from './UserContext.js';

function App() {

  const [user, setUser] = useState(null);

  const unSetUser = ()=>{
    localStorage.clear();
  }

  useEffect(()=>{

      fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then(data => {

        if(localStorage.getItem('token') !== null){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })

        }else{
          setUser(null);
        }
      })
  }, [])

  return (
    <UserProvider value={{user, setUser, unSetUser}}>
      <Router>
        <AppNavBar/>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/market" element={<Market/>}/>
          <Route path='/viewitem/:productId' element={<ViewItem/>}/>
          <Route path='*' element={<NotFound/>}/>
          <Route path="/logout" element={<Logout/>}/>

            <Route path="/cart" element={<Cart/>}/>
            <Route path='/image' element={<ViewImage/>}/>
            
            <Route path="/dashboard" element={<Dashboard/>}/>
            <Route path="/users" element={<Users/>}/>
            <Route path='/coupon/:couponCode' element={<UpdateCoupon/>}/>
            <Route path='/coupon' element={<AddCoupon/>}/>
            
        </Routes>
      </Router>
    </UserProvider>
  )
}

export default App;
