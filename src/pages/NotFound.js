
import {Link} from 'react-router-dom';
import {Container} from 'react-bootstrap';

export default function NotFound() {
	return (
		<Container className = "text-center mt-5">
			<h1>ERROR 404: PAGE NOT FOUND!</h1>
			<p>Page do not exist or you don't have access to the page.</p>
       		<Link to="/">Go back to the market page.</Link>
		</Container>
	)
}