import {Fragment,useEffect, useState} from 'react';
import {Row,Col,Tab,Tabs} from 'react-bootstrap';

import ItemCard from '../components/ItemCard.js';

export default function Market(){
	const [allProducts, setAllProducts] = useState([]);
	const [mens, setMens] = useState([]);
	const [womens, setWomens] = useState([]);
	const [toddlers, setToddlers] = useState([]);
	const [appliances, setAppliances] = useState([]);
	const [tools, setTools] = useState([]);
	const [others, setOthers] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/viewall`)
		.then(result => result.json())
		.then(data => {
			setAllProducts(data.map(product=>{
				return(
					<ItemCard key={product._id} productProp={product}/>
					)
				}))
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/mens`)
		.then(result => result.json())
		.then(data => {
			setMens(data.map(product=>{
				return(
					<ItemCard key={product._id} productProp={product}/>
					)
				}))
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/womens`)
		.then(result => result.json())
		.then(data => {
			setWomens(data.map(product=>{
				return(
					<ItemCard key={product._id} productProp={product}/>
					)
				}))
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/toddlers`)
		.then(result => result.json())
		.then(data => {
			setToddlers(data.map(product=>{
				return(
					<ItemCard key={product._id} productProp={product}/>
					)
				}))
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/appliances`)
		.then(result => result.json())
		.then(data => {
			setAppliances(data.map(product=>{
				return(
					<ItemCard key={product._id} productProp={product}/>
					)
				}))
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/tools`)
		.then(result => result.json())
		.then(data => {
			setTools(data.map(product=>{
				return(
					<ItemCard key={product._id} productProp={product}/>
					)
				}))
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/others`)
		.then(result => result.json())
		.then(data => {
			setOthers(data.map(product=>{
				return(
					<ItemCard key={product._id} productProp={product}/>
					)
				}))
			})
	}, [setAllProducts,setMens,setWomens,setToddlers,setAppliances,setTools,setOthers])


	return (
		<Fragment>
		<Col className="offset-2 col-8 mt-3">
		<Tabs defaultActiveKey="all" id="dashboard-tabs" className="text-center mb-3" fill>
		<Tab eventKey="all" title="All">
			<Row>
				{allProducts}
			</Row>
		</Tab>
		<Tab eventKey="mens" title="Mens">
			<Row>
				{mens}
			</Row>
		</Tab>
		<Tab eventKey="womens" title="Womens">
			<Row>
				{womens}
			</Row>
		</Tab>
		<Tab eventKey="toddlers" title="Toddlers">
			<Row>
				{toddlers}
			</Row>
		</Tab>
		<Tab eventKey="appliances" title="Appliances">
			<Row>
				{appliances}
			</Row>
		</Tab>
		<Tab eventKey="tools" title="Tools">
			<Row>
				{tools}
			</Row>
		</Tab>
		<Tab eventKey="others" title="Others">
			<Row>
				{others}
			</Row>
		</Tab>
    </Tabs>
    </Col>
		</Fragment>
		)
}