import {Fragment,useEffect, useState,useContext} from 'react';
import {Row,Col,Tab,Tabs,Table} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

import UserCard from '../components/UserCard.js';
import UserContext from '../UserContext.js';

export default function Users(){
	const {user} = useContext(UserContext);

	const [nonAdmin, setNonAdmin] = useState([]);
	const [admin, setAdmin] = useState([]);

	const [isClicked, setIsClicked] = useState(0);
	const navigate = useNavigate();

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/user/nonadmin`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setNonAdmin(data.map(viewnonadmin=>{
				return(
					<UserCard key={viewnonadmin._id} userProp={viewnonadmin}/>
					)
				}))
			})

		fetch(`${process.env.REACT_APP_API_URL}/user/admin`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setAdmin(data.map(viewadmin=>{
				return(
					<UserCard key={viewadmin._id} userProp={viewadmin}/>
					)
				}))
			})

	}, [setNonAdmin,setAdmin,setIsClicked])


	return (
		
		<Fragment>
		<Col className="offset-2 col-8 mt-3">
		<Tabs defaultActiveKey="nonadmin" id="users-tabs" className="text-center mb-3"  onClick={event => setIsClicked(isClicked+1)} fill>
		<Tab eventKey="nonadmin" title="Non-Admin">
		<Col className="mt-3">
			<h3 className="text-center">Non-Admin Users</h3>
		    <Table responsive striped bordered hover>
		      <thead>
		        <tr>
		          <th>User ID</th>
		          <th>User Email</th>
		          <th>Edit</th>
		        </tr>
		      </thead>
		      <tbody>
		      		{nonAdmin}
		      </tbody>
		    </Table>
		</Col>
		</Tab>
		<Tab eventKey="admin" title="Admin">
		<Col className="mt-3">
			<h3 className="text-center">Admin Users</h3>
		    <Table responsive striped bordered hover>
		      <thead>
		        <tr>
		          <th>User ID</th>
		          <th>User Email</th>
		          <th>Edit</th>
		        </tr>
		      </thead>
		      <tbody>
		      		{admin}
		      </tbody>
		    </Table>
		</Col>
		</Tab>
    </Tabs>
    </Col>
		</Fragment>
		)
}