import {Button, Form,Row,Col} from 'react-bootstrap';
import {Fragment} from 'react';
import {useState,useEffect,useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';
import Banner from '../components/Banner.js';
import Highlight from '../components/Highlight.js';

import Swal from 'sweetalert2';

export default function Home(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [newEmail, setNewEmail] = useState("");
	const [newPassword, setNewPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const navigate = useNavigate();
	const {user,setUser}=useContext(UserContext);

	const [logActive, setLogActive] = useState(false);

	useEffect(() => {
		if(email !== "" && password !== ""){
			setLogActive(true)
		}
		else {
			setLogActive(false)
		}
	}, [email,password])

	function login(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: 'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				Swal.fire({
					title: 'Authentication failed!',
					icon: 'error',
					text: 'Please try again!'
				})
			}else{
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'))

				Swal.fire({
					title: 'Authentication successful!',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})

				navigate("/market");
			}

		})
	}

	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if(newEmail !== "" && newPassword !== "" && confirmPassword !== "" && newPassword === confirmPassword){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [newEmail,newPassword,confirmPassword])

	function register(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method: 'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				email: newEmail,
				password: newPassword
			})
		})
		.then(result => result.json())
		.then(data => {

			if(data === false){
				Swal.fire({
					title: 'Registration failed!',
					icon: 'error',
					text: 'Please try again!'
				})
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
					method: 'POST',
					headers: {'Content-Type':'application/json'},
					body: JSON.stringify({
						email: newEmail,
						password: newPassword
					})

				})
				.then(result => result.json())
				.then(data => {

					localStorage.setItem('token', data.auth);
					retrieveUserDetails(localStorage.getItem('token'))

					Swal.fire({
						title: 'Registration successful!',
						icon: 'success',
						text: 'Welcome to Zuitt!'
					})

					navigate("/market");
				})
			}

		})
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return (
		user ?
		<Navigate to = "/market"/>
		:
		<Fragment>
		<Row className="offset-2 col-8">
		<Col className="col-8">
			<Banner/>
			<Highlight/>
		</Col>
		<Col className="mt-5">
		<Row className="bg-light rounded pb-3">
			<h3 className="text-center mt-3">Login</h3>
		    <Form className="mt-1" onSubmit={event => login(event)}>

		      <Form.Group className="mb-2" controlId="formBasicEmail">
		        <Form.Label>Email address:</Form.Label>
		        <Form.Control
		        	type="email" 
		        	placeholder="Enter email"
		        	value={email}
		        	onChange={event => setEmail(event.target.value)}
		        	required />
		        <Form.Text className="text-muted">
		          Login your account.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-2" controlId="formBasicPassword">
		        <Form.Label>Password:</Form.Label>
		        <Form.Control
		        	type="password"
		        	placeholder="Password"
		        	value={password}
		        	onChange={event => setPassword(event.target.value)}
		        	required />
		      </Form.Group>

		      {
		      	logActive ?
		      	<Button variant="primary" type="submit">
		      	  Login
		      	</Button>
		      	:
		      	<Button variant="danger" type="submit" disabled>
		      	  Login
		      	</Button>
		      }

		    </Form>
		    </Row>
		    <Row className="mt-3 bg-light rounded">
		    		<h4 className="text-center mt-2">Register</h4>
		    	    <Form className="mt-1 pb-3" onSubmit={event => register(event)}>

		    	      <Form.Group className="mb-2" controlId="formBasicNewEmail">
		    	        <Form.Label>Email address</Form.Label>
		    	        <Form.Control
		    	        	type="email" 
		    	        	placeholder="Enter email"
		    	        	value={newEmail}
		    	        	onChange={event => setNewEmail(event.target.value)}
		    	        	required />
		    	        <Form.Text className="text-muted">
		    	          Not yet registered? Register here!
		    	        </Form.Text>
		    	      </Form.Group>

		    	      <Form.Group className="mb-2" controlId="formBasicNewPassword">
		    	        <Form.Label>Password</Form.Label>
		    	        <Form.Control
		    	        	type="password"
		    	        	placeholder="Password"
		    	        	value={newPassword}
		    	        	onChange={event => setNewPassword(event.target.value)}
		    	        	required />
		    	      </Form.Group>

		    	      <Form.Group className="mb-2" controlId="formBasicConfirmPassword">
		    	        <Form.Label>Confirm Password</Form.Label>
		    	        <Form.Control
		    	        	type="password"
		    	        	placeholder="Confirm your password"
		    	        	value={confirmPassword}
		    	        	onChange={event => setConfirmPassword(event.target.value)}
		    	        	required />
		    	      </Form.Group>
		    	      {
		    	      	isActive ?
		    	      	<Button variant="primary" type="submit">
		    	      	  Submit
		    	      	</Button>
		    	      	:
		    	      	<Button variant="danger" type="submit" disabled>
		    	      	  Register
		    	      	</Button>
		    	      }

		    	    </Form>
		    </Row>
		</Col>
		</Row>
		</Fragment>
	)
}