import {Fragment,useEffect, useState,useContext} from 'react';
import {Table,Col,Row,Tab,Tabs} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';
import DashboardItems from '../components/DashboardItems.js';
import AddProduct from '../components/AddProduct.js';
import EditProduct from '../components/EditProduct.js';
import CartReceipts from '../components/CartReceipts.js';
import Coupon from '../components/Coupon.js';

export default function Dashboard(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [products, setProducts] = useState([]);
	const [archived, setArchived] = useState([]);
	const [isClicked, setIsClicked] = useState(0);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/viewarchived`)
		.then(result => result.json())
		.then(data => {
			setArchived(data.map(product=>{
				return(
					<DashboardItems key={product._id} productProp={product}/>
					)
				}))
			})
		fetch(`${process.env.REACT_APP_API_URL}/product/viewall`)
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(product=>{
				return(
					<DashboardItems key={product._id} productProp={product}/>
					)
				}))
			})
	}, [isClicked])


	return (
		
		<Fragment>
		<Row className="offset-2 col-8 mt-3">
		<Tabs defaultActiveKey="market" id="dashboard-tabs" className="text-center mb-3"  onClick={event => setIsClicked(isClicked+1)} fill>
		<Tab eventKey="market" title="Market">
		<Col className="mt-3">
			<h3 className="text-center">Market</h3>
		    <Table responsive striped bordered hover>
		      <thead>
		        <tr>
		          <th>Product ID</th>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Category</th>
		          <th>Price</th>
		          <th>Stocks</th>
		          <th>Sold</th>
		          <th>Discount</th>
		          {/*<th>Date Added</th>*/}
		          {/*<th>Image</th>*/}
		          <th>Edit</th>
		        </tr>
		      </thead>
		      <tbody>
		      		{products}
		      </tbody>
		    </Table>
		</Col>
		</Tab>
		<Tab eventKey="archived" title="Archived">
			<Col className="mt-3">
			<h3 className="text-center">Archived Products</h3>
		    <Table responsive striped bordered hover>
		      <thead>
		        <tr>
		          <th>Product ID</th>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Category</th>
		          <th>Price</th>
		          <th>Stocks</th>
		          <th>Sold</th>
		          <th>Discount</th>
		          {/*<th>Date Added</th>*/}
		          {/*<th>Image</th>*/}
		          <th>Edit</th>
		        </tr>
		      </thead>
		      <tbody>
		      		{archived}
		      </tbody>
		    </Table>
			</Col>
		</Tab>
		<Tab eventKey="orderlist" title="Checkouts">
			<Col>
			 <CartReceipts/>
			</Col>
		</Tab>
		<Tab eventKey="addproduct" title="Add Product">
			<Col className="offset-3 col-6">
			<AddProduct/>
			</Col>
		</Tab>
		<Tab eventKey="coupon" title="Coupon">
			<Col className="offset-2 col-8">
			<Coupon/>
			</Col>
		</Tab>
		<Tab eventKey="editproduct" title="Edit Product">
			<Col className="offset-3 col-6">
			<EditProduct/>
			</Col>
		</Tab>
    </Tabs>
    </Row>
		</Fragment>
		)
}