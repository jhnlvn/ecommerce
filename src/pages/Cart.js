import {Fragment,useEffect,useState,useContext} from 'react';
import {Col,Row,Form,Button, Card,ListGroup,Tab,Tabs} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';

import CartCard from '../components/CartCard.js'
import CartReceipts from '../components/CartReceipts.js'

import Swal from 'sweetalert2';

export default function Cart(){
	const {user} = useContext(UserContext);
	
	const [products, setProducts] = useState('');
	const [coupon, setCoupon] = useState('');
	const [total, setTotal] = useState('');
	const [payment, setPayment] = useState('cash-on-delivery');
	const [cartOut, setCartOut] = useState('');
	const [costumer, setCostumer] = useState('');
	const [address, setAddress] = useState('');
	const [contact, setContact] = useState('');

	const navigate = useNavigate();

	useEffect(()=>{

		fetch(`${process.env.REACT_APP_API_URL}/order/view`, 
		{
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setProducts(data.products.map(item=>{
				return(
					<CartCard key={item.productId} productProp={item}/>
					)
				}));
			setTotal(data.totalAmount);
		})
	}, [products]);


	function checkout(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/order/view`, {
			method: 'PUT',
			headers: {'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`},
			body: JSON.stringify({
				name: costumer,
				address: address,
				couponCode: coupon,
				contact: contact,
				paymentMethod: payment,
				isPurchased: true,
				purchasedOn: new Date()
			})
		})
		.then(result => result.json())
		.then(data => {

			if(data.purchasedOn === null){
				Swal.fire({
					title: 'Place order failed!',
					icon: 'error',
					text: 'Please try again!'
				})
			}else{

				Swal.fire({
					title: 'Checkout successful!',
					icon: 'success',
					text: 'Thank you!'
				})

				navigate("/market");
			}
		})
	}

	return (

		<Fragment>
		<Tabs defaultActiveKey="cart" id="cart-tabs" className="offset-2 col-8 text-center mt-3 mb-1" fill>
		<Tab eventKey="cart" title="Cart">
			<Row>
				<Col className="offset-2 col-4">
					<Card className="p-5">
					<h3 className="text-center">Checkout Card</h3>
						<Form className="text-center" onSubmit={event => checkout(event)}>

					      <Form.Group controlId="formCart">
							    <Form.Group controlId="formCostumer">
							    <Form.Label>Contact Name:</Form.Label>
					      		<Form.Control type="text" className="text-center" placeholder="Name of the receiver." onChange={event => setCostumer(event.target.value)} required/>
							    </Form.Group>

					       		<Form.Group controlId="formContact">
					       		<Form.Label className="mt-3">Contact Number:</Form.Label>
					       		<Form.Control type="phone" className="text-center" placeholder="What is your phone number?" onChange={event => setContact(event.target.value)} required/>
					       		</Form.Group>

					       		<Form.Group controlId="formAddress">
					       		<Form.Label className="mt-3">Delivery Address:</Form.Label>
					       		<Form.Control as="textarea" className="text-center" style={{ height: '100px' }} placeholder="Location of the delivery" onChange={event => setAddress(event.target.value)} required/>
					       		</Form.Group>

					       		<Form.Group controlId="formCoupon">
					       		<Form.Label className="mt-3">Discount Coupon:</Form.Label>
					       		<Form.Control type="text" className="mx-auto text-center"  style={{ width: '100px' }} onChange={event => setCoupon(event.target.value)}/>
					       		</Form.Group>

							    <h6 className="mt-3">Payment Method:</h6>
					        	<ListGroup defaultActiveKey="cod" required className="offset-3 text-center col-6 mb-3">
					        	  <ListGroup.Item eventKey="cod" value="Cash-On-Delivery" onClick={event => setPayment("cash-on-delivery")}>
					        	    Cash On Delivery
					        	  </ListGroup.Item>
					        	  <ListGroup.Item eventKey="gcash" value="G-Cash" onClick={event => setPayment("g-cash")}>
					        	    G-Cash
					        	  </ListGroup.Item>
					        	  <ListGroup.Item eventKey="online-padala" value="Online-Padala" onClick={event => setPayment("online-padala")}>
					        	    Online-Padala
					        	  </ListGroup.Item>
					        	</ListGroup>

							    <h5>Total Amount:</h5>
							    <h2>PhP {total}.00</h2>
							</Form.Group>
							<Button variant="primary" type="submit">Checkout Now!</Button>
						</Form>
				    </Card>
				</Col>
				<Col className="col-4 mt-3">
					<h3 className="text-center mb-2">Item List</h3>
					{products}
				</Col>
			</Row>
		</Tab>
		<Tab eventKey="receipts" title="Receipts">
			<Row>
			<Col className="mx-auto col-8">
			 <CartReceipts/>
			</Col>
			</Row>
		</Tab>
		</Tabs>
		</Fragment>
		)
}